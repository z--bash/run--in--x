#!/bin/bash
# Example: runInX.sh -r '1366x768' -s -w '/home/user/Games/Lego\ Star\ Wars' -t 3 -b '/home/user/Games/Lego\ Star\ Wars/bin/mountCd.sh' -a '/home/user/Games/Lego\ Star\ Wars/bin/unmountCd.sh' '/home/user/Games/Lego\ Star\ Wars/bin/start.sh'
# Basic dependencies: bspwm, sxhkd, xinit
# Steam optional dependencies: lsi-steam (or change Steam prefix below to anything else)

STEAM_PREFIX='lsi-' # STEAM_PREFIX=''
MAX_TERM=6 # default value on most systems

#=#=#=#
MAIN_COMMAND="$STEAM_PREFIX\steam -bigpicture" # default: "steam -bigpicture", otherwise - game executable
RESOLUTION="" # default: "" - native resolution, "AxB" - custom resolution
WORKPATH="" # default: "" - same directory as executable, "/absolute/path" - custom workpath
TERMINAL=2 # default: 2, tty 2
START_COMMAND="" # default not set, ex: mount disk image
END_COMMAND="" # default not set, ex: unmount disk image

OPTIND=1 # Reset in case getopts has been used previously in the shell. # A POSIX variable

function show_help {
	echo '--- [Help] ---'
	echo 'This is an incomplete help section...'
	echo '--- masterTimMi, 2018 ---'
}
echo '-------------------------'
while getopts ":r:p:t:b:a:h:" opt; do
    case "$opt" in
    r  )
		RESOLUTION="$OPTARG"
		echo "| -r: custom resolution | $RESOLUTION"
        ;;
	p  )
		WORKPATH="$OPTARG"
		echo "| -p: custom workpath   | $WORKPATH"
		;;
	t  )
		TERMINAL="$OPTARG"
		if [[ ! "$TERMINAL" > "1" && ! "$TERMINAL" < "$MAX_TERM" ]]; then
			echo "| [e] Invalid terminal chosen. Available terminals: 2 - $MAX_TERM"
			exit 1
		fi
		echo "| -t: custom terminal    | $TERMINAL"
		;;
	b  )
		START_COMMAND="$OPTARG"
		echo "| -b: command before     | $START_COMMAND"
		;;
	a  )
		END_COMMAND="$OPTARG"
		echo "| -a: command after      | $END_COMMAND"
		;;
	\? )
		echo "| [e] Invalid argument   | -$OPTARG" >&2
		echo 'Run with -h argument for help.'
		exit 1
		;;
	:  )
		echo "Option -$OPTARG requires an argument." >&2
		exit 1
		;;
	h|*)
        show_help
        exit 0
        ;;
    esac
done
shift $((OPTIND-1))

MAIN_COMMAND=$1
echo "| The main command      : $MAIN_COMMAND"

if [[ ! "$RESOLUTION" = "" ]]; then
	RESOLUTION="xrandr -s $RESOLUTION &&"
fi

if [[ ! "$WORKPATH" = "" ]]; then
	DIR=$(dirname "${MAIN_COMMAND}")
	if [ "$(echo "$DIR" | cut -f 1 -d " ")" = "steam" ]; then # echo "A Steam game"
		
		#if ! pgrep -x "steam" > /dev/null; then # echo "Steam was started"
		#	lsi-steam &> /dev/null & echo "Steam was started"
		#else # echo "Steam is already running"
		#	echo "Steam is running"
		#fi
		
		GAMEROOT="$STEAM_PREFIX"
	else # echo "NOT a Steam game"
		export PATH=$PATH:$DIR
		GAMEROOT="cd '$DIR' && "
		MAIN_COMMAND=$(basename "${MAIN_COMMAND}")
	fi
fi
echo '-------------------------'
echo "THIS screen [Ctrl+Alt+F1]"
echo "GAME screen [Ctrl+Alt+F$TERMINAL]"
echo "STOP game server [Ctrl+C]"
echo
echo "Preparing environment..."

/bin/sh -c "$START_COMMAND"

read -n 1 -p "Ready to start? Press any key to switch to game"
echo
echo
echo "The game is running. Press [Ctrl+Alt+F$TERMINAL] to play"

xinit /bin/sh -c "\
(xterm -e 'echo \"THIS screen [Ctrl+Alt+F$TERMINAL]\"; echo \"HOME screen [Ctrl+Alt+F1]\"; read -p \"STOP game server [Enter]\" && kill -9 $$') && \
sxhkd & bspwm & xsetroot -cursor_name left_ptr && unclutter --timeout 3 -b && \ 
$NEW_RES ($GAMEROOT$MAIN_COMMAND; wait) && wait\
" -- :1 -nolisten tcp vt$TERMINAL #&>/dev/null

echo
read -n 1 -p "Finished. Press any key to exit";echo &&

/bin/sh -c "$END_COMMAND" &>/dev/null
